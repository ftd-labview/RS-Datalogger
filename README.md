# Rapid Sampler datalogger

A simple LabVIEW datalogger for the TU Delft CSE Rapid Sampling setup.



## Software Requirements

LabVIEW 2015 or later

## License

This project is licenced under the [EUPL Licence](LICENCE.md).

## Contact

For any questions or inquiries, please contact Dirk Geerts (d.j.m.geerts@tudelft.nl).
